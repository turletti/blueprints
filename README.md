<table style="border-collapse: collapse; border: none;">
  <tr style="border-collapse: collapse; border: none;">
    <td style="border-collapse: collapse; border: none;">
      <a href="http://www.openairinterface.org/">
         <img src="./images/oai_final_logo.png" alt="" border=3 height=50 width=150>
         </img>
      </a>
    </td>
    <td style="border-collapse: collapse; border: none; vertical-align: center;">
      <b><font size = "5">OpenAirInterface Blueprints</font></b>
    </td>
  </tr>
</table>

This repository contains blueprints for different use-cases where OAI components can be used. 

Below is the list of available blueprints

1. [OpenAirInterface Multi-access Edge Computing Platform](./mep/README.md)